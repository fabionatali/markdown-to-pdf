---
author: Jane Doe
title: Markdown to PDF toolchain
subtitle: A typesetting system for minimalists
date: 10 January 2017
---

# Markdown to PDF toolchain

## Introduction

This Markdown to PDF toolchain can be used to convert a Markdown file
into a nicely-formatted PDF. It is meant as a typesetting system for
minimalists. Under the hood, the toolchain relies on Pandoc [@pandoc]
and \LaTeX [@latex].

All standard Markdown formatting should be accepted, see the
cheatsheet below for a complete list of options.

The layout and formatting of the final PDF can be customised modifying
the `template.tex` file or directly inserting \LaTeX code into the
Markdown file.

## Installation

On a Debian system make sure the following dependencies are installed.

    apt-get install pandoc texlive-xetex

No need to install the toolchain, simply create a local copy of this
folder.

## Usage

Edit the file `project.md`, then run `make` on the command line to
create the PDF.

## Cheatsheet

### Fonts

    *Italic*, **bold** and `inline code`.

*Italic*, **bold** and `inline code`.

### Links

    This is a [link](https://example.com).

This is a [link](https://example.com).

### External references

    You can cite external sources [@lorem, p. 42] and ...

You can cite external sources [@lorem, p. 42] and have a reference
section automatically added at the end of the PDF. References must be
added to the `biblio.bib` file in the BibTeX format.

### Quotes

    > Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
    > faucibus ligula vel blandit suscipit. Fusce vitae sem mattis,
    > convallis risus dapibus, varius sapien.

> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
> faucibus ligula vel blandit suscipit. Fusce vitae sem mattis,
> convallis risus dapibus, varius sapien.

### Code

    ```
    s = "Python syntax highlighting"
    print s
    ```

```
s = "Python syntax highlighting"
print s
```

### Images

    ![A caption](img/example.png "An image"){width=50%}

![A caption](img/example.png "An image"){width=50%}
