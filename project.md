---
category: Internal documentation
title: Employee handbook
subtitle: For John Donne
author: Jane Doe
date: 2021-01-01
confidentiality: Medium confidentiality not for further distribution
---

# Executive summary

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus.

Nulla posuere. Donec vitae dolor. Nullam tristique diam non turpis. Cras
placerat accumsan nulla. Nullam rutrum.  Nam vestibulum accumsan nisl.

## Lorem ipsum

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel
tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean in sem ac
leo mollis blandit. Donec neque quam, dignissim in, mollis nec, sagittis eu,
wisi. Phasellus lacus.

## Aliquam serat solus

Mauris mollis tincidunt felis. Aliquam feugiat tellus ut neque. Nulla facilisis,
risus a rhoncus fermentum, tellus tellus lacinia purus, et dictum nunc justo sit
amet elit.

# Integer placerat

Etiam laoreet quam sed arcu. Phasellus at dui in ligula
mollis ultricies. Integer placerat tristique nisl. Praesent augue. Fusce
commodo. Vestibulum convallis, lorem a tempus semper, dui dui euismod elit,
vitae placerat urna tortor vitae lacus. Nullam libero mauris, consequat quis,
varius et, dictum id, arcu. Mauris mollis tincidunt felis. Aliquam feugiat
tellus ut neque. Nulla facilisis, risus a rhoncus fermentum, tellus tellus
lacinia purus, et dictum nunc justo sit amet elit.

## Sed dolor

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor
tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis
eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae
dolor. Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam
rutrum. Nam vestibulum accumsan nisl.

## Suspendisse potenti

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel
tortor sodales tellus ultricies commodo. Suspendisse potenti.

- Aenean in sem ac leo mollis blandit
- Donec neque quam, dignissim in, mollis
- Nec, sagittis eu, wisi, phasellus lacus

Etiam laoreet quam sed arcu. Phasellus at dui in ligula mollis
ultricies. Integer placerat tristique nisl. Praesent augue. Fusce
commodo. Vestibulum convallis, lorem a tempus semper, dui dui euismod elit,
vitae placerat urna tortor vitae lacus. Nullam libero mauris, consequat quis,
varius et, dictum id, arcu. Mauris mollis tincidunt felis. Aliquam feugiat
tellus ut neque. Nulla facilisis, risus a rhoncus fermentum, tellus tellus
lacinia purus, et dictum nunc justo sit amet elit.

## Urna tortor vitae lacus

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel
tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean in sem ac
leo mollis blandit.

> Donec neque quam, dignissim in, mollis nec, sagittis eu, wisi. Phasellus
> lacus. Etiam laoreet quam sed arcu. Phasellus at dui in ligula mollis
> ultricies. Integer placerat tristique nisl. Praesent augue.

Fusce commodo. Vestibulum convallis, lorem a tempus semper, dui dui euismod
elit, vitae placerat urna tortor vitae lacus. Nullam libero mauris, consequat
quis, varius et, dictum id, arcu.

```
n = int(input('Type a number, and its factorial will be printed: '))

if n < 0:
    raise ValueError('You must enter a non negative integer')

factorial = 1
for i in range(2, n + 1):
    factorial *= i

print(factorial)
```

[Source](https://en.wikipedia.org/wiki/Python_(programming_language)#Programming_examples).

Mauris mollis tincidunt felis. Aliquam feugiat tellus ut neque. Nulla facilisis,
risus a rhoncus fermentum, tellus tellus lacinia purus, et dictum nunc justo sit
amet elit.
