# XeLaTeX must be called multiple times for everything to be displayed
# correctly
pdf: project.md template.tex img/background.pdf
	cat project.md | \
	pandoc \
	  --pdf-engine=xelatex \
	  --output project.tex \
	  --top-level-division=chapter \
	  --template template.tex && \
	xelatex project.tex && \
	xelatex project.tex && \
	xelatex project.tex

img/background.pdf: img/background.cfdg
	cfdg --size=200x200 --svg img/background.cfdg /tmp/background.svg && \
	cat /tmp/background.svg | \
	inkscape -w 600 -h 600 --pipe --export-filename img/background.pdf && \
	rm -fr /tmp/background.svg

clean:
	rm -fr \
	  *.aux \
	  *.log \
	  *.toc \
	  *.bbl \
	  *.bcf \
	  *.blg \
	  *.out \
	  *.run.xml \
	  *~ \
	  .*~ \
	  project.tex

.PHONY: clean
