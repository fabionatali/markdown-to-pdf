project.pdf: project.tex
	xelatex project.tex
	biber project
	xelatex project.tex
	xelatex project.tex

project.tex: project.md template.tex biblio.bib
	pandoc --biblatex --latex-engine=xelatex project.md -o project.tex --template template.tex

clean:
	rm -fr project.aux project.log project.toc project.bbl project.bcf \
	       project.blg project.out project.run.xml project.tex
