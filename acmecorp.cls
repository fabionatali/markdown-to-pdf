%-------------------------------------------------------------------------------
% Fabio Natali, 2019-04-07
%
% License: Public Domain
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% CLASS CONFIGURATION
%-------------------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{acmecorp}[2019/04/07 A LaTeX class for Acme Corp]
\LoadClass[10pt,a4paper,oneside]{book}
\usepackage[utf8]{inputenc}

%-------------------------------------------------------------------------------
% VARIABLES
%-------------------------------------------------------------------------------

% Define the class variables, eg usage: \title{<title>}
\renewcommand*{\title}[1]{\def\@title{#1}}
\newcommand*{\subtitle}[1]{\def\@subtitle{#1}}
\newcommand*{\category}[1]{\def\@category{#1}}
\newcommand*{\confidentiality}[1]{\def\@confidentiality{#1}}
% Usage: \date{2000-12-31}
\usepackage[en-GB]{datetime2}
\renewcommand*{\date}[1]{\def\@date{\DTMdate{#1}}}

%-------------------------------------------------------------------------------
% PAGE
%-------------------------------------------------------------------------------

\usepackage{geometry}
\geometry{
  paper=a4paper,
  top=3cm,
  bottom=3cm,
  left=2.2cm,
  right=2.2cm,
  headheight=0.75cm,
  footskip=1cm,
  footnotesep=1cm,
  headsep=1cm,
  % showframe,
}

%-------------------------------------------------------------------------------
% CUSTOM COLOURS
%-------------------------------------------------------------------------------

\usepackage[table]{xcolor}
\definecolor{AcmeCorpLightGray}{RGB}{100, 100, 100}
\definecolor{AcmeCorpGray}{RGB}{40, 40, 40}

%-------------------------------------------------------------------------------
% TYPOGRAPHY
%-------------------------------------------------------------------------------

\usepackage[T1]{fontenc}
\usepackage{fontspec}
\setmainfont[Color=AcmeCorpGray]{IBM Plex Sans}
\setsansfont[Color=AcmeCorpGray]{IBM Plex Sans}
\setmonofont[Color=AcmeCorpGray]{IBM Plex Mono}
\usepackage[english]{babel}
\newfontfamily\highlighted[Color=AcmeCorpGray]{IBM Plex Mono Medium}
\newfontfamily\condensed[Color=AcmeCorpGray]{IBM Plex Sans Condensed}

%-------------------------------------------------------------------------------
% PARAGRAPHS
%-------------------------------------------------------------------------------

\usepackage{setspace}
\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt}
\renewcommand{\baselinestretch}{1.2}

%-------------------------------------------------------------------------------
% HEADERS AND FOOTERS
%-------------------------------------------------------------------------------

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{} % Remove everything
\renewcommand{\footrulewidth}{0pt}
\renewcommand{\headrulewidth}{0pt}

\lhead{\footnotesize \@category}
\rhead{\footnotesize \@title}
\lfoot{\footnotesize \@confidentiality}
\rfoot{\footnotesize \thepage}

\fancypagestyle{plain}{%
  \lhead{}
  \rhead{}
}

%-------------------------------------------------------------------------------
% SECTION HEADINGS
%-------------------------------------------------------------------------------

\usepackage[compact]{titlesec} % must come before `hyperref` (to avoid wrong links in ToC)
\titleformat{name=\chapter}[hang]
            {\Huge\highlighted}
            {}
            {0em}
            {}
\titleformat{name=\section}[hang]
            {\Large\highlighted}
            {}
            {0em}
            {}

%-------------------------------------------------------------------------------
% TABLE OF CONTENTS
%-------------------------------------------------------------------------------

\usepackage{titletoc}

\titlecontents{chapter}[0pt]
              {\addvspace{4mm}\large}
              {\thecontentslabel\enspace}
              {\contentsmargin{0pt}}
              {\small\titlerule*[1em]{.}\large\contentspage}

\titlecontents{section}[0pt]
              {\addvspace{-1.5mm}\small}
              {\thecontentslabel\enspace}
              {\contentsmargin{0pt}}
              {\titlerule*[1em]{.}\contentspage}

%-------------------------------------------------------------------------------
% FIGURES
%-------------------------------------------------------------------------------

\usepackage{float}
\floatplacement{figure}{H}

%-------------------------------------------------------------------------------
% LISTS
%-------------------------------------------------------------------------------

\usepackage{enumitem}
\setlist[itemize]{
  label={-}
}

%-------------------------------------------------------------------------------
% VERBATIM
%-------------------------------------------------------------------------------

\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage[framemethod=tikz]{mdframed}
\mdfdefinestyle{VerbatimFrameStyle}{
  topline=false,
  bottomline=false,
  leftline=true,
  rightline=false,
  linewidth=2mm,
  linecolor=gray!20,
  innerrightmargin=3mm,
  innerleftmargin=3mm,
  innertopmargin=3mm,
  innerbottommargin=3mm,
  leftmargin=0mm,
  rightmargin=0mm,
  skipabove=3mm,
  skipbelow=3mm,
  roundcorner=0
}
\let\verbatimold\verbatim
\renewenvironment{verbatim}{
  \begin{mdframed}[style=VerbatimFrameStyle]
    \begin{minipage}[c]{\textwidth}
      \begin{verbatimold}
}
{
      \end{verbatimold}
    \end{minipage}
  \end{mdframed}
}

%-------------------------------------------------------------------------------
% FRONT PAGE
%-------------------------------------------------------------------------------

\usepackage{eso-pic}

\newcommand{\titlepagegeometry}{%
  \newgeometry{
    top=2cm,
    bottom=2cm,
    left=2.2cm,
    right=2.2cm,
    headheight=0cm,
    footskip=0cm,
    headsep=0cm,
  }
}

\newcommand{\logo}{%
  \setlength{\fboxsep}{3mm}
  \colorbox{AcmeCorpGray}{
    \includegraphics[width=40mm]{img/logo.pdf}
  }
}

\newcommand{\titlesection}{%
  \begin{minipage}{.9\linewidth}
    \raggedright
    \Large\MakeUppercase{\@category}
    \begin{spacing}{1}
      \Huge{\highlighted{\@title}}
    \end{spacing}
    \vspace{-2mm}
    \Large\@subtitle
  \end{minipage}
}

\renewcommand*{\maketitle}{
  \titlepagegeometry

  \begin{titlepage}
    \AddToShipoutPicture*{\put(-150,360){%
        \includegraphics[width=2\textwidth]{img/background}
    }}

    \begin{tikzpicture}[remember picture, overlay]
      \draw[line width=1mm, color=AcmeCorpGray]
      ($(current page.north west)+(2.2,-2)$)
      rectangle
      ($(current page.north east)-(2.2,18.6)$);
      \node[inner sep=0pt, anchor=north east] (logo) at
      ($(current page.north east)-(2.2,2)$) {\logo};
      \node[inner sep=2.5mm, anchor=south west] (title) at
      ($(current page.north west)+(2.2,-18.6)$) {\titlesection};
    \end{tikzpicture}

    \vspace*{\stretch{1}}

    \begin{minipage}[b]{.6\textwidth}
      \footnotesize
      Acme Corp Ltd\\
      Registered in Gryffindor, Company Number 000000123\\
      105 Crown Street, London, WX01 2YZ
    \end{minipage}%
    \begin{minipage}[b]{.4\textwidth}
      \raggedleft\Large\texttt\@date
    \end{minipage}
  \end{titlepage}
  \restoregeometry
}

%-------------------------------------------------------------------------------
% URLS
%-------------------------------------------------------------------------------

\usepackage[hang,flushmargin,bottom]{footmisc}
\setlength{\footnotemargin}{3.2mm}
\usepackage[hyphens]{url}
\usepackage[hyperfootnotes=false,hidelinks]{hyperref}
\let\hrefold\href
\renewcommand{\href}[2]{\underline{\hrefold{#1}{#2}}\footnote{\url{#1}}}

%-------------------------------------------------------------------------------
% MISC
%-------------------------------------------------------------------------------

% Images, by default preserve the original aspect ratio
\usepackage{graphicx}
\setkeys{Gin}{keepaspectratio}

% Tables
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{tabularx}
\renewcommand{\arraystretch}{1.5}
\newcolumntype{L}{>{\raggedright\arraybackslash}X}
\newcolumntype{R}{>{\raggedleft\arraybackslash}X}
\arrayrulecolor{AcmeCorpLightGray!30}

% Fix Pandoc issue, see
% https://tex.stackexchange.com/questions/257418/error-tightlist-converting-md-file-into-pdf-using-pandoc
\def\tightlist{}
