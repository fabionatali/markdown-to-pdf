# A simple markdown-to-PDF toolchain

This repository contains a simple markdown-to-PDF toolchain, based on Pandoc,
LaTeX, and Context Free (an image generator, see
[here](https://www.contextfreeart.org/)). A _Makefile_ is provided to facilitate
the build process.

## Install and build

On a reasonably up-to-date Debian GNU/Linux operating system, it should be
possible to run the makefile, provided that the following dependencies are
installed.

    apt install pandoc texlive-xetex fonts-ibm-plex contextfree

To create a PDF version of the document, type `make` from within this
repository's root folder.

## Notes

`fonts-ibm-plex` is in Debian _contrib_, as opposed to _main_, as it seems to
involve non-free software as part of its build process. You may want to consider
switching to a different font.
